import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Login from './Login.js';
import CreateIncident from './CreateIncident.js';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {BrowserRouter as Router, Route,Switch} from 'react-router-dom';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
  <MuiThemeProvider>
    <Router>
      <Switch>
        
    			<Route path="/" exact  component={Login} />
          <Route path="/viewIncidents"  component={App} />
    			<Route path="/createIncident" component={CreateIncident}/>
        
      </Switch>
    </Router>
	</MuiThemeProvider>, document.getElementById('root'));
registerServiceWorker();
