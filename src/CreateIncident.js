import React from 'react';
import Subheader from 'material-ui/Subheader';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {Link} from 'react-router-dom';
import RaisedButton from 'material-ui/RaisedButton';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';

export default class CreateIncident extends React.Component {
  state = {
    value: 1,
  };

  handleChange = (event, index, value) => this.setState({value});
  render() {
    return (
      <div>        
        <Subheader style={{background: 'rgb(0, 188, 212)',
                                fontSize: '28px',
                                color: 'white',
                                marginTop: '1px',
                                marginLeft: '-7px'}}>
                                Create Incident / Trouble Ticket
                                </Subheader>
        <center>
          <Table selectable={false} multiSelectable={false}>
            <TableBody displayRowCheckbox={false}>
              <TableRow displayBorder={false}>
                <TableRowColumn><label>Service Account</label></TableRowColumn>
                <TableRowColumn>
                  <SelectField value={this.state.value} onChange={this.handleChange}>
                    <MenuItem value={1} primaryText="9702072793" />
                    <MenuItem value={2} primaryText="9702072793" />
                  </SelectField>
                </TableRowColumn>
              </TableRow>
              <TableRow displayBorder={false}>
                <TableRowColumn>Incident Type</TableRowColumn>
                <TableRowColumn>
                  <SelectField value={this.state.value} onChange={this.handleChange}>
                    <MenuItem value={1} primaryText="Service Activation" />
                    <MenuItem value={2} primaryText="Add-On Activation" />
                    <MenuItem value={3} primaryText="Service Barred" />
                  </SelectField>
                </TableRowColumn>
              </TableRow>
              <TableRow displayBorder={false}>
                <TableRowColumn>Incident Comment</TableRowColumn>
                <TableRowColumn>
                  <TextField hintText="Comments"/>
                </TableRowColumn>
              </TableRow>
              <TableRow>
                <TableRowColumn>
                  <Link to="/viewIncidents">
                     <RaisedButton label="Submit" primary={true} style={style}/>
                  </Link>
                </TableRowColumn>
              </TableRow>
            </TableBody>
          </Table>
        </center>
      </div>
    );
  }
  }
const style = {
 margin: 15
};