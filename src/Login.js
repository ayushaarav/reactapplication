import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import {Link} from 'react-router-dom';
import injectTapEventPlugin  from 'react-tap-event-plugin';
injectTapEventPlugin();
class Login extends Component {
  constructor(props){
  super(props);
  this.state={
  username:'',
  password:''
  }
 }
  render() {
         return (
         <div>
        <MuiThemeProvider>
          <div>
          <AppBar showMenuIconButton={false}
             title="Customer Login Portal Demo"
           />
           <center>
           <TextField
             hintText="Enter your Username"
             floatingLabelText="Login"
             onChange = {(event,newValue) => this.setState({username:newValue})}
             />
           <br/>
             <TextField
               type="password"
               hintText="Enter your Password/OTP"
               floatingLabelText="Password/OTP"
               onChange = {(event,newValue) => this.setState({password:newValue})}
               />
             <br/>
             <Link to="/viewIncidents"><RaisedButton label="Submit" primary={true} style={style}/></Link>
             </center>
         </div>
         </MuiThemeProvider>
      </div>
      );
    }
}
const style = {
 margin: 15
};
export default Login;
