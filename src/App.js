import React, { Component } from 'react';
import $ from 'jquery';
import ViewMap from './ViewMap';
import Subheader from 'material-ui/Subheader';
import IconButton from 'material-ui/IconButton';
import AddCircleOutline from 'material-ui/svg-icons/content/add-circle-outline';
import RaisedButton from 'material-ui/RaisedButton';
import {Link} from 'react-router-dom';
import './App.css';
import jsondata from './incident.json';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      incidents: [], dataEmpty: 0, flag: false
    };
  }
  RecallIncident = () =>{
     this.setState({incidents: jsondata, dataEmpty: jsondata.length, flag: true });
  };
  componentDidMount = () => {
  this.RecallIncident();
  console.log(jsondata);
  };
  render() {
         return(
           <div>
            {this.state.flag && this.state.dataEmpty === 0 ?
            //Conditional statements (if data is empty display button to create incident otherwise display incidents)
            <div>
              <center>
                <div style={{marginTop: '200px'}}><h2 >You are yet to create a Incident...</h2>
                  <Link to="/createIncident"><RaisedButton label="Create"
                        buttonStyle={{backgroundColor: '#5CA59F  '}}/></Link>
                </div>
              </center>
             </div> :
             //display incidents
              <div>
                  <Subheader style={{background: 'rgb(0, 188, 212)',
                                     fontSize: '28px',
                                     color: 'white',
                                     marginTop: '1px',
                                     marginLeft: '-7px'}}>
                                     Incident</Subheader>
                  <Link to="/createIncident">
                      <IconButton tooltip="Add Incident" style={{float: 'right',
                         marginTop: '-47px', marginRight: '20px'}} iconStyle={{fontSize: '36px'}}>
                        <AddCircleOutline/>
                      </IconButton>
                  </Link>
                  <ViewMap incidents={this.state.incidents} />
              </div>}
           </div>
           );
    }
}
export default App;
